//! \file
//! \brief Compatibility HAL GPIO driver for STM32F1xx

#ifndef HAL_COMPAT_GPIO_HPP_
#define HAL_COMPAT_GPIO_HPP_

#include "stm32f1xx_ll_gpio.h"

#include "utils.hpp"

namespace stm32f1xx
{

namespace compat
{

//!
enum class gpio_port
{
    a,
    b,
    c,
    d,
    e,
};

//!
enum class gpio_mode
{
    input           =   LL_GPIO_MODE_INPUT,
    output          =   LL_GPIO_MODE_OUTPUT,
    analog          =   LL_GPIO_MODE_ANALOG,
    alternate       =   LL_GPIO_MODE_ALTERNATE,
};

//!
enum class gpio_pull
{
    up              =   LL_GPIO_PULL_UP,
    down            =   LL_GPIO_PULL_DOWN,
};

//!
enum class gpio_edge
{
    falling,
    rising,
    both,
};

//!
class gpio_default_exti
{
    static constexpr unsigned pin_to_exti_channel(int pin)
    {
        switch (pin) {
            case LL_GPIO_PIN_0:     return LL_EXTI_LINE_0;
            case LL_GPIO_PIN_1:     return LL_EXTI_LINE_1;
            case LL_GPIO_PIN_2:     return LL_EXTI_LINE_2;
            case LL_GPIO_PIN_3:     return LL_EXTI_LINE_3;
            case LL_GPIO_PIN_4:     return LL_EXTI_LINE_4;
            case LL_GPIO_PIN_5:     return LL_EXTI_LINE_5;
            case LL_GPIO_PIN_6:     return LL_EXTI_LINE_6;
            case LL_GPIO_PIN_7:     return LL_EXTI_LINE_7;
            case LL_GPIO_PIN_8:     return LL_EXTI_LINE_8;
            case LL_GPIO_PIN_9:     return LL_EXTI_LINE_9;
            case LL_GPIO_PIN_10:    return LL_EXTI_LINE_10;
            case LL_GPIO_PIN_11:    return LL_EXTI_LINE_11;
            case LL_GPIO_PIN_12:    return LL_EXTI_LINE_12;
            case LL_GPIO_PIN_13:    return LL_EXTI_LINE_13;
            case LL_GPIO_PIN_14:    return LL_EXTI_LINE_14;
            case LL_GPIO_PIN_15:    return LL_EXTI_LINE_15;
            default:                return -1;
        }
    }

public:
    template<gpio_port Port, unsigned Pin>
    static void irq_mask(gpio_edge edge = gpio_edge::both)
    {
        constexpr auto ch = pin_to_exti_channel(Pin);
        if (edge == gpio_edge::both) {
            LL_EXTI_DisableFallingTrig_0_31(ch);
            LL_EXTI_DisableRisingTrig_0_31(ch);
        } else if (edge == gpio_edge::rising) {
            LL_EXTI_DisableRisingTrig_0_31(ch);
        } else if (edge == gpio_edge::falling) {
            LL_EXTI_DisableFallingTrig_0_31(ch);
        }
    }

    template<gpio_port Port, unsigned Pin>
    static void irq_unmask(gpio_edge edge)
    {
        constexpr auto ch = pin_to_exti_channel(Pin);
        if (edge == gpio_edge::both) {
            LL_EXTI_EnableFallingTrig_0_31(ch);
            LL_EXTI_EnableRisingTrig_0_31(ch);
        } else if (edge == gpio_edge::rising) {
            LL_EXTI_EnableRisingTrig_0_31(ch);
        } else if (edge == gpio_edge::falling) {
            LL_EXTI_EnableFallingTrig_0_31(ch);
        }
    }

    template<gpio_port Port, unsigned Pin>
    static void irq_clear()
    {
        constexpr auto ch = pin_to_exti_channel(Pin);
        LL_EXTI_ClearFlag_0_31(ch);
    }

};

//!
// TODO: assign GPIO_LL_EC_PIN values to Pins (make an enum) and use constexpr cast (as in theCore)
template<gpio_port Port, unsigned ...Pins>
class gpio
{
public:
    //!
    static void set()
    {
        auto port = get_gpio_port();
        LL_GPIO_SetOutputPin(port, concatenate_pins<Pins...>());
    }

    //!
    static void reset()
    {
        auto port = get_gpio_port();
        LL_GPIO_ResetOutputPin(port, concatenate_pins<Pins...>());
    }

    //!
    static uint32_t read()
    {
        auto port = get_gpio_port();
        return LL_GPIO_IsInputPinSet(port, concatenate_pins<Pins...>());
    }

    //!
    static void toggle()
    {
        auto port = get_gpio_port();
        LL_GPIO_TogglePin(port, concatenate_pins<Pins...>());
    }

protected:
    template<unsigned ...P>
    static constexpr unsigned concatenate_pins()
    {
        return ( ... | P);
    }

    //!
    static GPIO_TypeDef *get_gpio_port()
    {
        if constexpr(Port == gpio_port::a) return GPIOA;
        if constexpr(Port == gpio_port::b) return GPIOB;
        if constexpr(Port == gpio_port::c) return GPIOC;
        if constexpr(Port == gpio_port::d) return GPIOD;
        if constexpr(Port == gpio_port::e) return GPIOE;
    }
};

//!
// There is a requirement - only one pin per one LL_GPIO_SetPinMode call
template<gpio_port Port, unsigned Pin, class ExtiControl = gpio_default_exti>
class gpio_pin : public gpio<Port, Pin>
{
public:
    //!
    static void set_mode(gpio_mode mode)
    {
        auto port = gpio<Port, Pin>::get_gpio_port();
        auto int_mode = lib::extract_value(mode);
        LL_GPIO_SetPinMode(port, Pin, int_mode);
    }

    //!
    static void set_pull(gpio_pull pull)
    {
        auto port = gpio<Port, Pin>::get_gpio_port();
        auto int_pull = lib::extract_value(pull);
        LL_GPIO_SetPinPull(port, Pin, int_pull);
    }

    static void irq_mask(gpio_edge edge = gpio_edge::both)
    {
        ExtiControl::template irq_mask<Port, Pin>(edge);
    }

    static void irq_unmask(gpio_edge edge)
    {
        ExtiControl::template irq_unmask<Port, Pin>(edge);
    }

    static void irq_clear()
    {
        ExtiControl::template irq_clear<Port, Pin>();
    }
};


} // namespace compat

} // namespace stm32f1xx

//! Alias
namespace platform = stm32f1xx;

#endif // HAL_COMPAT_GPIO_HPP_