//! \file
//! \brief

#ifndef HAL_COMPAT_PLATFORM_HPP_
#define HAL_COMPAT_PLATFORM_HPP_

#include "gpio.hpp"
#include "console.hpp"
#include "uart.hpp"
#include "platform_utils.hpp"

#endif // HAL_COMPAT_PLATFORM_HPP_