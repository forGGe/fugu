//! \file
//! \brief

// TODO: wrap it into namespace

namespace stm32f1xx
{

namespace compat
{

//! Initializes a console according to the configuration provided
void console_cfg();

//! Puts a character into the UART console device
static inline void bypass_putc(char c)
{
    while (!LL_USART_IsActiveFlag_TC(CONSOLE_UART)) { }
    LL_USART_TransmitData8(CONSOLE_UART, c);
}

} // namespace compat

} // namespace stm32f1xx

//! Alias
namespace platform = stm32f1xx;
