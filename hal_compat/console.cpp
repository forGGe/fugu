//! \file
//! \brief Console utilities for STM32F1 MCU

#include "stm32f1xx_ll_usart.h"
#include "stm32f1xx_ll_bus.h"

#include <errno.h>
#include <sys/unistd.h> // STDOUT_FILENO, STDERR_FILENO
#include <stdio.h>

namespace stm32f1xx
{

namespace compat
{

void console_cfg()
{
    // TODO: rewrite it to direct LL calls, without struct
#ifdef CONSOLE_UART
    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1);
    LL_USART_InitTypeDef USART_InitStruct;


    USART_InitStruct.BaudRate = CONSOLE_BAUD;
    USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
    USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
    USART_InitStruct.Parity = LL_USART_PARITY_NONE;
    USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
    USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
    LL_USART_Init(CONSOLE_UART, &USART_InitStruct);

    LL_USART_ConfigAsyncMode(CONSOLE_UART);

    LL_USART_Enable(CONSOLE_UART);
#endif // CONSOLE_UART
}

} // namespace compat

} // namespace stm32f1xx

#ifdef CONSOLE_UART
extern "C"
__attribute__ ((__used__))
int _write(int file, char *data, int len)
{
    if ((file != STDOUT_FILENO) && (file != STDERR_FILENO))
    {
        errno = EBADF;
        return -1;
    }

    // arbitrary timeout 1000
    for (int i = 0; i < len; ++i) {
        while (!LL_USART_IsActiveFlag_TC(CONSOLE_UART)) { }
        LL_USART_TransmitData8(CONSOLE_UART, data[i]);
    }

    // return # of bytes written - as best we can tell
    return len;
}
#endif
