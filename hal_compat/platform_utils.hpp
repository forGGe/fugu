//! \file
//! \brief

#ifndef HAL_COMPAT_UTILS_HPP_
#define HAL_COMPAT_UTILS_HPP_

#include "stm32f1xx_ll_utils.h"

namespace stm32f1xx
{

namespace compat
{

void delay(int ms)
{
    LL_mDelay(ms);
}

} // namespace compat

} // namespace stm32f1xx

//! Alias
namespace platform = stm32f1xx;

#endif // HAL_COMPAT_UTILS_HPP_