set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_VERSION 1)

#-------------------------------------------------------------------------------
# Toolchain definitions

# Find a compiler
find_program(GCC_PATH arm-none-eabi-gcc)
find_program(GXX_PATH arm-none-eabi-g++)

# GCC is used as a linker in order to use LTO properly
set(CMAKE_C_LINKER ${GCC_PATH})
set(CMAKE_CXX_LINKER ${GXX_PATH})

# specify the cross compiler
set(CMAKE_C_COMPILER ${GCC_PATH})
set(CMAKE_CXX_COMPILER ${GXX_PATH})

# Way to avoid specifying linking flags in the toolchain.
# See https://cmake.org/pipermail/cmake-developers/2016-February/027888.html
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

# there is no target environment
set(CMAKE_FIND_ROOT_PATH "")

# search for programs in the build host directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

#-------------------------------------------------------------------------------
# Flags and definitions used with GNU complilation suite

# Essential stuff for using 3rd-party libs
set(CMAKE_INCLUDE_SYSTEM_FLAG_C "-isystem ")
set(CMAKE_INCLUDE_SYSTEM_FLAG_CXX "-isystem ")

# avoid using any additional flags when linking with shared libraries
set(CMAKE_SHARED_LIBRARY_LINK_C_FLAGS "")

# common flags for current platform
set(CC_PLATFORM_FLAGS "-mcpu=cortex-m3 -mthumb -fdata-sections -ffunction-sections")

# -fno-use-cxa-atexit helps resolve issue with DSO handle undefined reference
# why????
set(CXX_PLATFORM_FLAGS "-fno-use-cxa-atexit -fno-exceptions -fno-rtti ${CC_PLATFORM_FLAGS}")

# TODO: move std and gdwarf flags out of toolchain into the core listfile itself
set(C_CXX_EXTRA_FLAGS "")
set(CC_EXTRA_FLAGS "-std=c99 ${C_CXX_EXTRA_FLAGS}")
set(CXX_EXTRA_FLAGS "-std=c++1z ${C_CXX_EXTRA_FLAGS}")

# Set general flags for C\C++ compiler and linker
set(CC_WARN_FLAGS "-Wall -Wextra -Wpedantic -Werror -Wno-error=register")
set(CXX_WARN_FLAGS "${CC_WARN_FLAGS} -Weffc++")

# Supported modes are normal, release, debug and minimum size
# Normal mode
set(CMAKE_C_FLAGS
    "${CMAKE_C_FLAGS} ${CC_PLATFORM_FLAGS} ${CC_WARN_FLAGS} ${CC_EXTRA_FLAGS}"
    CACHE STRING "C flags")
set(CMAKE_CXX_FLAGS
    "${CMAKE_CXX_FLAGS} ${CXX_PLATFORM_FLAGS} ${CXX_WARN_FLAGS} ${CXX_EXTRA_FLAGS}"
    CACHE STRING "C++ flags")

# Release flags, optimization is on,
set(CMAKE_C_FLAGS_RELEASE "-O3 -flto=4 -DNDEBUG "
    CACHE STRING "Release C flags")
set(CMAKE_CXX_FLAGS_RELEASE ${CMAKE_C_FLAGS_RELEASE}
    CACHE STRING "Release C++ flags")

# Minimum size release flags, LTO and minimum size
set(CMAKE_C_FLAGS_MINSIZEREL "-Os -flto -DNDEBUG "
    CACHE STRING "Minsize C flags")
set(CMAKE_CXX_FLAGS_MINSIZEREL ${CMAKE_C_FLAGS_MINSIZEREL}
    CACHE STRING "Minsize C++ flags")

# Debug mode, no LTO and maximum debug info
set(CMAKE_C_FLAGS_DEBUG  "-O0 -gdwarf-2 -g3 -ggdb3"
    CACHE STRING "Debug C flags")
set(CMAKE_CXX_FLAGS_DEBUG ${CMAKE_C_FLAGS_DEBUG}
    CACHE STRING "Debug C++ flags")

set(CMAKE_OBJCOPY arm-none-eabi-objcopy CACHE STRING "Objcopy executable")

set(CMAKE_ASM-ATT_COMPILE_OBJECT
    "<CMAKE_ASM-ATT_COMPILER> -mcpu=cortex-m3 -o <OBJECT> <SOURCE>")
set(CMAKE_C_LINK_EXECUTABLE
    "${CMAKE_C_LINKER} <OBJECTS> <LINK_LIBRARIES> <CMAKE_C_LINK_FLAGS>  -o <TARGET>")
set(CMAKE_CXX_LINK_EXECUTABLE
    "${CMAKE_CXX_LINKER} <OBJECTS> <LINK_LIBRARIES> <CMAKE_CXX_LINK_FLAGS> -o <TARGET>")
