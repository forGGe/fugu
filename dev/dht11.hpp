//! \file
//! \brief

#ifndef DHT11_DEV_DRIVER_HPP_
#define DHT11_DEV_DRIVER_HPP_

#include "platform.hpp"
#include <stdio.h>
#include <stdint.h>

namespace dev
{

//!
enum dht11_status
{
    data_ready,
    in_progress,
    crc_bad,
    failed
};

//! DHT11 driver implementation using timers and GPIO interrupts.
//! \details
//! Single-bus data format is used for communication and synchronization between MCU and
//! DHT11 sensor. One communication process is about 4ms.
//! Data consists of decimal and integral parts. A complete data transmission is 40bit, and the
//! sensor sends higher data bit first.
//! Data format: 8bit integral RH data + 8bit decimal RH data + 8bit integral T data + 8bit decimal T
//! data + 8bit check sum. If the data transmission is right, the check-sum should be the last 8bit of
//! "8bit integral RH data + 8bit decimal RH data + 8bit integral T data + 8bit decimal T data".
template<class GpioChannel, class TimerChannel>
class dht11
{
public:
    //!
    static int init();

    //!
    static int start_read();

    //!
    static int abort_read();

    //!
    static dht11_status data_status();

    //!
    static int get_temperature();

    //!
    static int get_humidity();

    //--------------------------------------------------------------------------

    //!
    static void event_gpio();

    //!
    static void event_timer();

private:
    //!
    enum class state
    {
        invalid,
        inited,
        req_start,
        wait_data,
        data_done,
    };

    //!
    static state m_state;

    //!
    static int m_recv;

    //! Pulse bits, received from sensor.
    //! \details 42 bits: 1 initial MCU bit + 1 sensor ack bit + 40 bits of data
    //! (including checksum).
    static int64_t m_bits;
};

template<class GpioChannel, class TimerChannel>
typename dht11<GpioChannel, TimerChannel>::state dht11<GpioChannel, TimerChannel>::m_state;

template<class GpioChannel, class TimerChannel>
int dht11<GpioChannel, TimerChannel>::m_recv;

template<class GpioChannel, class TimerChannel>
int64_t dht11<GpioChannel, TimerChannel>::m_bits;

//------------------------------------------------------------------------------

//!
template<class GpioChannel, class TimerChannel>
int dht11<GpioChannel, TimerChannel>::init()
{
    // TODO: assert if already inited

    GpioChannel::irq_mask();
    GpioChannel::set_mode(platform::compat::gpio_mode::output);
    GpioChannel::set();
    m_state = state::inited;
    return 0;
}

//!
template<class GpioChannel, class TimerChannel>
int dht11<GpioChannel, TimerChannel>::start_read()
{
    // TODO: assert if not inited or already reading

    m_recv = 0;
    m_bits = 0;
    GpioChannel::reset();
    m_state = state::req_start;
    TimerChannel::one_shot(18);

    return 0;
}

//!
template<class GpioChannel, class TimerChannel>
int dht11<GpioChannel, TimerChannel>::abort_read()
{
    // TODO: return error if not inited or not reading

    GpioChannel::irq_mask();
    TimerChannel::stop();
    GpioChannel::set_mode(platform::compat::gpio_mode::output);
    GpioChannel::set();

    m_state = state::inited;

    return 0;
}

template<class GpioChannel, class TimerChannel>
dht11_status dht11<GpioChannel, TimerChannel>::data_status()
{
    // TODO: assert if not inited or not data done

    // Soleley for debug purpose.
    // TODO: find more elegant way to handle it
#if 1
    printf("\r\n");
    for (int i = 0; i < m_recv; ++i) {
        printf("%-3d ", (int)((m_bits >> (42 - i)) & 0x1));
    }
    printf("\r\n");
    printf("%x\r\n", (int)(m_bits & 0x3));
#endif

    if (m_recv != 42) {
        return dht11_status::in_progress;
    }

    // First two MSB must be MCU request finish ('0') and ACK ('1')
    if (((m_bits >> 40) & 0x3) != 0x2) {
        return dht11_status::failed;
    }

    // Calculate checksum, for that wraparound is required

    // Extract 32 bits of MSB data
    uint32_t data = (uint32_t)(m_bits >> 8) & 0xffffffff;

    // Extract checksum, LSB 8 bits
    uint8_t chksum = (uint8_t)m_bits;

    // Compute checksum
    uint8_t comp_chksum = (data & 0xff)
        + ((data >> 8) & 0xff)
        + ((data >> 16) & 0xff)
        + ((data >> 24) & 0xff);

    if (chksum != comp_chksum) {
        return dht11_status::crc_bad;
    }

    return dht11_status::data_ready;
}

template<class GpioChannel, class TimerChannel>
int dht11<GpioChannel, TimerChannel>::get_temperature()
{
    // TODO: assert if not inited or not data done

    int32_t data = (m_bits >> 8) & 0xffffffff;

    // By design, DHT11 will return not more than 16 bits of temperature data
    return (int)data & 0xffff;
}

template<class GpioChannel, class TimerChannel>
int dht11<GpioChannel, TimerChannel>::get_humidity()
{
    // TODO: assert if not inited or not data done

    int32_t data = (m_bits >> 8) & 0xffffffff;

    // By design, DHT11 will return not more than 16 bits of humidity data
    return (int)(data >> 16) & 0xffff;
}

//------------------------------------------------------------------------------

template<class GpioChannel, class TimerChannel>
void dht11<GpioChannel, TimerChannel>::event_timer()
{
    if (m_state != state::req_start) {
        for(;;); // TODO: full assert
    }

    m_state = state::wait_data;

    GpioChannel::set_mode(platform::compat::gpio_mode::input);
    TimerChannel::start();
    GpioChannel::set_pull(platform::compat::gpio_pull::up);
    GpioChannel::irq_unmask(platform::compat::gpio_edge::falling);
}

template<class GpioChannel, class TimerChannel>
void dht11<GpioChannel, TimerChannel>::event_gpio()
{
    if (m_state != state::wait_data) {
        for(;;); // TODO: full assert
    }

    auto passed = TimerChannel::us_from_start();
    TimerChannel::reset();

    // Pulse width can vary across sensors.
    // Empirically, value of 100 was chosen
    if (passed >= 100) {
        // 42 - m_recv swaps bit order right away
        m_bits |= ((int64_t)1 << (42 - m_recv));
    }

    // Interrupt is serviced
    GpioChannel::irq_clear();

    m_recv++;

    if (m_recv == 42) {
        GpioChannel::irq_mask();
        // Just in case
        GpioChannel::irq_clear();

        TimerChannel::stop();
        GpioChannel::set_mode(platform::compat::gpio_mode::output);
        GpioChannel::set();

        m_state = state::data_done;
    }

// Soleley for debug purpose.
// TODO: find more elegant way to handle it
#if 0
    static int times[43];
    times[m_recv] = passed;
#endif

// Soleley for debug purpose.
// TODO: find more elegant way to handle it
#if 0
    if (m_recv == 42) {
        printf("\r\n");
        for (int i = 0; i <= 42; ++i) {
            printf("%-3d ", (int)((m_bits >> i) & 0x1));
        }
        printf("\r\n");
        for (int i = 0; i <= 42; ++i) {
            printf("%-3d ", times[i]);
        }
        printf("\r\n");
    }
#endif
}

} // dev

#endif // DHT11_DEV_DRIVER_HPP_