#ifndef FUGU_PROJECT_CONFIG_H_
#define FUGU_PROJECT_CONFIG_H_

//! MCU model
#define STM32F103xB                         1
//! Use LL driver from STM32 HAL
#define USE_FULL_LL_DRIVER                  1
//! HSE clock value
#define HSE_VALUE                           8000000U
//! HSE startup timeout
#define HSE_STARTUP_TIMEOUT                 100U
//! LSE startup timeout
#define LSE_STARTUP_TIMEOUT                 5000U
//! LSE clock value
#define LSE_VALUE                           32768U
//! HSI clock value
#define HSI_VALUE                           8000000U
//! LSI clock value
#define LSI_VALUE                           40000U
//! VDD value
#define VDD_VALUE                           3300U
//! Prefetch?
#define PREFETCH_ENABLE                     32768U
//! UART periphery used for console output
#define CONSOLE_UART                        USART1
//! Console baud rate
#define CONSOLE_BAUD                        115200

#endif // FUGU_PROJECT_CONFIG_H_