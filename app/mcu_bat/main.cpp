#include "stm32f1xx.h"
#include "stm32f1xx_ll_bus.h"
#include "stm32f1xx_ll_cortex.h"
#include "stm32f1xx_ll_dma.h"
#include "stm32f1xx_ll_exti.h"
#include "stm32f1xx_ll_gpio.h"
#include "stm32f1xx_ll_pwr.h"
#include "stm32f1xx_ll_rcc.h"
#include "stm32f1xx_ll_system.h"
#include "stm32f1xx_ll_usart.h"
#include "stm32f1xx_ll_utils.h"

#include "console.hpp"

#include <stdio.h>

//------------------------------------------------------------------------------
// Low-level initialization

static void ll_init()
{
    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_AFIO);
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);

    // NOJTAG: JTAG-DP Disabled and SW-DP Enabled
    LL_GPIO_AF_Remap_SWJ_NOJTAG();
}

//------------------------------------------------------------------------------
// Clock configuration

static void clk_config()
{
    LL_FLASH_SetLatency(LL_FLASH_LATENCY_0);

    if (LL_FLASH_GetLatency() != LL_FLASH_LATENCY_0) {
        for (;;)
            ; // TODO: assert
    }
    LL_RCC_HSI_SetCalibTrimming(16);

    // Running from HSI
    LL_RCC_HSI_Enable();

    // Wait till HSI is ready
    while (LL_RCC_HSI_IsReady() != 1) {
    }

    LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
    LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
    LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
    LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_HSI);

    // Wait till System clock is ready
    while (LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_HSI) {
    }

    LL_Init1msTick(8000000);
    LL_SYSTICK_SetClkSource(LL_SYSTICK_CLKSOURCE_HCLK);
    LL_SetSystemCoreClock(8000000);
}

//------------------------------------------------------------------------------
// Pin configuration

void pin_config()
{
    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOC | LL_APB2_GRP1_PERIPH_GPIOA);

    // PC13 - LED

    LL_GPIO_ResetOutputPin(GPIOC, LL_GPIO_PIN_13);
    LL_GPIO_SetPinMode(GPIOC, LL_GPIO_PIN_13, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(GPIOC, LL_GPIO_PIN_13, LL_GPIO_OUTPUT_PUSHPULL);

    LL_GPIO_AF_DisableRemap_USART1();

    // USART1 - console

    // PA9 - TX

    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_9, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_9, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_9, LL_GPIO_PULL_UP);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_9, LL_GPIO_SPEED_FREQ_HIGH);

    // PA10 - RX

    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_10, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_10, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_10, LL_GPIO_PULL_UP);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_10, LL_GPIO_SPEED_FREQ_HIGH);
}

//------------------------------------------------------------------------------
// Main routine

int main()
{
    ll_init();
    clk_config();
    pin_config();
    console_cfg();

    while (1) {
        LL_mDelay(100);
        LL_GPIO_SetOutputPin(GPIOC, LL_GPIO_PIN_13);
        LL_mDelay(700);
        LL_GPIO_ResetOutputPin(GPIOC, LL_GPIO_PIN_13);
        printf("Hello, World!\r\n");
    }
}

//------------------------------------------------------------------------------
// Cortex-M3 interrupts

//! Non maskable interrupt handler
void NMI_Handler(void)
{
}

//!
void HardFault_Handler(void)
{
    while (1) {
    }
}

//!
void MemManage_Handler(void)
{
    while (1) {
    }
}

//!
void BusFault_Handler(void)
{
    while (1) {
    }
}

//!
void UsageFault_Handler(void)
{
    while (1) {
    }
}

//!
void SVC_Handler(void)
{
}

//!
void DebugMon_Handler(void)
{
}

//!
void PendSV_Handler(void)
{
}

//!
void SysTick_Handler(void)
{
}

//------------------------------------------------------------------------------
// STM32F1XX interrupts