#include "stm32f1xx.h"
#include "stm32f1xx_ll_bus.h"
#include "stm32f1xx_ll_cortex.h"
#include "stm32f1xx_ll_dma.h"
#include "stm32f1xx_ll_exti.h"
#include "stm32f1xx_ll_pwr.h"
#include "stm32f1xx_ll_rcc.h"
#include "stm32f1xx_ll_rtc.h"
#include "stm32f1xx_ll_system.h"
#include "stm32f1xx_ll_tim.h"
#include "stm32f1xx_ll_usart.h"
#include "stm32f1xx_ll_utils.h"

#include "dht11.hpp"
#include "platform.hpp"

#include <stdio.h>

// Present exclusively to help resolve IDE parsing issues
#ifdef IDE_HELPER_SWITCH
#include "project_config.h"
#endif

//------------------------------------------------------------------------------
// Clock calculation helpers

//! Gets system clock value in compile-time
static constexpr auto get_sysclk_value()
{
    static_assert(SYSCLOCK_SOURCE == LL_RCC_SYS_CLKSOURCE_PLL ||
                  SYSCLOCK_SOURCE == LL_RCC_SYS_CLKSOURCE_HSE);

    if (SYSCLOCK_SOURCE == LL_RCC_SYS_CLKSOURCE_PLL) {

        // Do a division/multiplication, as PLL does

        auto clk = HSE_VALUE;

        if constexpr (PLL_HSE_DIV == LL_RCC_PLLSOURCE_HSE_DIV_1) {
            clk = clk;
        } else if (PLL_HSE_DIV == LL_RCC_PLLSOURCE_HSE_DIV_2) {
            clk /= 2;
        } else {
            return 0U; // To be able to test using static assertion
        }

        if constexpr (PLL_MUL == LL_RCC_PLL_MUL_2) {
            clk *= 2;
        } else if (PLL_MUL == LL_RCC_PLL_MUL_3) {
            clk *= 3;
        } else if (PLL_MUL == LL_RCC_PLL_MUL_4) {
            clk *= 4;
        } else if (PLL_MUL == LL_RCC_PLL_MUL_5) {
            clk *= 5;
        } else if (PLL_MUL == LL_RCC_PLL_MUL_6) {
            clk *= 6;
        } else if (PLL_MUL == LL_RCC_PLL_MUL_7) {
            clk *= 7;
        } else if (PLL_MUL == LL_RCC_PLL_MUL_8) {
            clk *= 8;
        } else {
            return 0U; // To be able to test using static assertion
        }

        return clk;

    } else if (SYSCLOCK_SOURCE == LL_RCC_SYS_CLKSOURCE_HSE) {
        return HSE_VALUE;
    }

    return 0U; // To be able to test using static assertion
}

//! Gets APB1 clock in compile time
static constexpr auto get_apb1_clk()
{
    constexpr auto sysclk = get_sysclk_value();
    switch (APB1_PRESCALER) {
        case LL_RCC_APB1_DIV_1:
            return sysclk / 1;
        case LL_RCC_APB1_DIV_2:
            return sysclk / 2;
        case LL_RCC_APB1_DIV_4:
            return sysclk / 4;
        default:
            return 0U;
    }
}

//! Gets APB1 timers value in compile time
static constexpr auto get_apb1_tim_clk()
{
    constexpr auto apb1_clk = get_apb1_clk();
    return APB1_PRESCALER == LL_RCC_APB1_DIV_1 ? apb1_clk : apb1_clk * 2;
}

static_assert(get_sysclk_value() != 0, "Sysclock calculation failed");
static_assert(get_apb1_clk() != 0, "APB1 clock calculation failed");
static_assert(get_apb1_tim_clk() != 0, "APB1 timer clock calculation failed");

//------------------------------------------------------------------------------

//! Low-level initialization
static void ll_init()
{
    // TODO: figure out importance of following

    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_AFIO);
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);

    // Enable BKP CLK enable for backup registers. TODO: decide if really
    // required
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_BKP);

    // TODO: check importance of following calls

    LL_RTC_TAMPER_Disable(BKP);
    LL_RTC_SetOutputSource(BKP, LL_RTC_CALIB_OUTPUT_SECOND);

    // NOJTAG: JTAG-DP Disabled and SW-DP Enabled
    LL_GPIO_AF_Remap_SWJ_NOJTAG();
}

//------------------------------------------------------------------------------

//! Clock configuration
static void clk_config()
{
    LL_FLASH_SetLatency(LL_FLASH_LATENCY_0);

    if (LL_FLASH_GetLatency() != LL_FLASH_LATENCY_0) {
        for (;;)
            ; // TODO: assert
    }

    //--------------------------------------------------------------------------
    // HSE management

    // Running from HSE
    LL_RCC_HSE_Enable();

    // Wait till HSE is ready
    while (LL_RCC_HSE_IsReady() != 1) {
    }

    // TODO: decide if following is important

    LL_PWR_EnableBkUpAccess();
    LL_RCC_ForceBackupDomainReset();
    LL_RCC_ReleaseBackupDomainReset();

    //--------------------------------------------------------------------------
    // LSE management

    LL_RCC_LSE_Enable();

    // Wait till LSE is ready
    while (LL_RCC_LSE_IsReady() != 1) {
    }

    //--------------------------------------------------------------------------
    // PLL management (if required)

    if constexpr (SYSCLOCK_SOURCE == LL_RCC_SYS_CLKSOURCE_PLL) {
        // PLL must be configured before enabling

        // SysClock domain configuration
        LL_RCC_PLL_ConfigDomain_SYS(PLL_HSE_DIV, PLL_MUL);

        // Main source - HSE
        // This will also set PREDIV1SRC bit
        LL_RCC_PLL_SetMainSource(LL_RCC_PLLSOURCE_HSE);

        LL_RCC_PLL_Enable();

        // Wait till PLL is ready
        while (LL_RCC_PLL_IsReady() != 1) {
        }
    }

    //--------------------------------------------------------------------------
    // Clock domains control

    LL_RCC_SetSysClkSource(SYSCLOCK_SOURCE);
    LL_RCC_SetRTCClockSource(LL_RCC_RTC_CLKSOURCE_LSE);
    LL_RCC_SetAHBPrescaler(AHB_PRESCALER);
    LL_RCC_SetAPB1Prescaler(APB1_PRESCALER);
    LL_RCC_SetAPB2Prescaler(APB2_PRESCALER);

    // Wait till System clock is ready
    while (LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL) {
    }

    //--------------------------------------------------------------------------
    // System and timer clock calculation

    if constexpr (SYSCLOCK_SOURCE == LL_RCC_SYS_CLKSOURCE_PLL) {
    }

    LL_SetSystemCoreClock(get_sysclk_value());
    LL_SYSTICK_SetClkSource(LL_SYSTICK_CLKSOURCE_HCLK);

    // TODO: make sure systick interrupt is not used anywhere.
    LL_Init1msTick(get_sysclk_value());
}

//------------------------------------------------------------------------------

//! Pin configuration
static void pin_config()
{
    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOC |
                             LL_APB2_GRP1_PERIPH_GPIOA |
                             LL_APB2_GRP1_PERIPH_GPIOB);

    // PB9 - DHT11

    LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_9);
    LL_GPIO_SetPinMode(GPIOB, LL_GPIO_PIN_9, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(GPIOB, LL_GPIO_PIN_9, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull(GPIOB, LL_GPIO_PIN_9, LL_GPIO_PULL_UP);
    LL_GPIO_SetPinSpeed(GPIOB, LL_GPIO_PIN_9, LL_GPIO_SPEED_FREQ_HIGH);

    // Enable IRQ for PB9 (EXTI line 5 .. 9)

    NVIC_EnableIRQ(EXTI9_5_IRQn);
    LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_9);
    LL_GPIO_AF_SetEXTISource(LL_GPIO_AF_EXTI_PORTB, LL_GPIO_AF_EXTI_LINE9);

    // PC13 - LED

    LL_GPIO_ResetOutputPin(GPIOC, LL_GPIO_PIN_13);
    LL_GPIO_SetPinMode(GPIOC, LL_GPIO_PIN_13, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(GPIOC, LL_GPIO_PIN_13, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull(GPIOC, LL_GPIO_PIN_13, LL_GPIO_PULL_UP);
    LL_GPIO_SetPinSpeed(GPIOC, LL_GPIO_PIN_13, LL_GPIO_SPEED_FREQ_LOW);

    // USART1 - console

    // PA9 - TX

    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_9, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_9, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_9, LL_GPIO_PULL_UP);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_9, LL_GPIO_SPEED_FREQ_HIGH);

    // PA10 - RX

    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_10, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_10, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_10, LL_GPIO_PULL_UP);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_10, LL_GPIO_SPEED_FREQ_HIGH);
}

//------------------------------------------------------------------------------

//! RTC configuration
static void rtc_init()
{
    // Peripheral clock enable
    LL_RCC_EnableRTC();

    LL_RTC_DisableWriteProtection(RTC);

    if (LL_RTC_WaitForSynchro(RTC) == ERROR) {
        for (;;)
            ; // TODO: full assert
    }

    if (LL_RTC_EnterInitMode(RTC) == ERROR) {
        for (;;)
            ; // TODO: full assert
    }

    // TODO: decide if required
    LL_RTC_ClearFlag_ALR(RTC);
    LL_RTC_ClearFlag_OW(RTC);
    LL_RTC_ClearFlag_SEC(RTC);

    // Prescaler will be reloaded once per 32768 ticks of LSE, which is
    // in turn, has a frequency of 32.768 kHz.
    // This will result in step of 1 second / prescaler reload
    LL_RTC_SetAsynchPrescaler(RTC, 32768);

    // Reset a time
    LL_RTC_TIME_Set(RTC, 0);

    LL_RTC_ExitInitMode(RTC);

    LL_RTC_EnableWriteProtection(RTC);
}

//------------------------------------------------------------------------------

//! Timer configuration
static void timer_init()
{
    LL_APB1_GRP1_EnableClock(TEST_TIMER_PERIPH_CLK | DHT_TIMER_PERIPH_CLK);

    //--------------------------------------------------------------------------
    // Helpers for timer value calculations

    auto calc_presc_value = [](auto tick_period_us) constexpr 
    {
        return get_apb1_tim_clk() / 1000000ULL * tick_period_us;
    };

    auto calc_reload_value = [](auto tick_period_us, auto desired_us) constexpr
    {
        return desired_us / tick_period_us;
    };

    constexpr auto tim_apb1_clk = get_apb1_tim_clk();

    // Minimum possible tick frequency, nanoseconds
    constexpr auto min_ns = 1000000000ULL / tim_apb1_clk;
    // Maximum possible tick frequency, nanoseconds
    constexpr auto max_ns = 1000000000ULL / tim_apb1_clk * 65536;

    //--------------------------------------------------------------------------

    // Timer 2 - test timer. Reload period is meant to be equal to 500 ms.

    // max prescaler == 1^16, timer clk == X MHz, means that
    // min tick freq == X MHz / 65536.
    // max tick freq == X Mhz
    // min tick period = 1 / X MHz
    // max tick period = 65536 / X MHz
    // desired timer tick period must lie between those values

    {
        // Tick period in nanoseconds
        constexpr auto tick_ns = TEST_TIMER_TICK_PERIOD_US * 1000U;
        static_assert(
          tick_ns > min_ns && tick_ns < max_ns,
          "Selected tick values can't be achieved with current clock speed");
    }

    // A timer tick frequency then will be:
    // 1 / T, where T is tick period. Thus, prescaler value == T * X
    constexpr auto test_presc = calc_presc_value(TEST_TIMER_TICK_PERIOD_US);
    static_assert(test_presc <= 65536);
    LL_TIM_SetPrescaler(TEST_TIMER, test_presc);

    // As noted above, 500 ms reload period is required.
    // 500 ms * 1000 / T us (tick period in microseconds) == reload register value.
    LL_TIM_SetAutoReload(TEST_TIMER, calc_reload_value(TEST_TIMER_TICK_PERIOD_US, 500 * 1000));

    // No real implications for this testing, just for convenience
    LL_TIM_SetCounterMode(TEST_TIMER, LL_TIM_COUNTERMODE_UP);

    // Trigger update to load new values into respective registers
    LL_TIM_GenerateEvent_UPDATE(TEST_TIMER);

    // Enable update event
    // **BUG** in LL library:
    // According to RM0008, page 403, section 15.4.1 TIMx control register 1
    // (TIMx_CR1) UDIS bit must be reset to let the timer update event be
    // enabled. Instead of clearing that bit, the LL_TIM_EnableUpdateEvent()
    // function sets it.
    CLEAR_BIT(TEST_TIMER->CR1, TIM_CR1_UDIS);

    // Clear update flag to prevent interrupt fire
    LL_TIM_ClearFlag_UPDATE(TEST_TIMER);

    // Enable interrupt as an action for any timer event
    LL_TIM_EnableIT_UPDATE(TEST_TIMER);

    // Enable NVIC interrupt
    NVIC_EnableIRQ(TEST_TIMER_IRQn);

    //--------------------------------------------------------------------------

    // Timer 3 - DHT11 timer

    {
        // Tick period in nanoseconds
        constexpr auto tick_ns = DHT_TIMER_TICK_PERIOD_US * 1000U;
        static_assert(
          tick_ns > min_ns && tick_ns < max_ns,
          "Selected tick values can't be achieved with current clock speed");
    }

    // DHT11 requires time interrupts of ~ 20 ms (for sending MCU to DHT11 data
    // request) and measuring time with roughly 1 to 10 us precision 
    // (measure '1' - 70-80 us pulse or '0' - 25 us pulse).
    constexpr auto dht11_presc = calc_presc_value(DHT_TIMER_TICK_PERIOD_US);
    static_assert(dht11_presc <= 65536);
    LL_TIM_SetPrescaler(DHT_TIMER, dht11_presc);

    // Counting up to measure time conveniently
    LL_TIM_SetCounterMode(DHT_TIMER, LL_TIM_COUNTERMODE_UP);

    // Deliver IRQs to DHT11 driver
    NVIC_EnableIRQ(DHT_TIMER_IRQn);

    // Rest of timer configuration will be executed on demand in timer classes
}

//------------------------------------------------------------------------------

// App-specific definitions

//!
struct basic_dht11_timer
{
    //!
    static void start()
    {
        // Pick highest value available, we don't care about underflow/overflow
        // events (for now).
        // TODO: catch underflow or overflow and report it to driver.
        LL_TIM_SetAutoReload(DHT_TIMER, 0xffff);

        // Trigger update to load new values into respective registers
        LL_TIM_GenerateEvent_UPDATE(DHT_TIMER);

        // Disable event generation
        // **BUG** in LL library:
        // According to RM0008, page 403, section 15.4.1 TIMx control register 1
        // (TIMx_CR1) UDIS bit must be set to disable the timer update event.
        // Instead of setting that bit, the LL_TIM_DisableUpdateEvent() function
        // clears it.
        SET_BIT(DHT_TIMER->CR1, TIM_CR1_UDIS);

        // Disable interrupt as an action for timer events
        LL_TIM_DisableIT_UPDATE(DHT_TIMER);

        // Reset a counter to predictable value
        LL_TIM_SetCounter(DHT_TIMER, 0);

        // Start timer
        LL_TIM_EnableCounter(DHT_TIMER);
    }

    //!
    // Implies reset
    static void stop()
    {
        // Stop counting
        LL_TIM_DisableCounter(DHT_TIMER);
        LL_TIM_SetCounter(DHT_TIMER, 0);
    }

    //!
    static void reset()
    {
        LL_TIM_SetCounter(DHT_TIMER, 0);
        // Trigger update to load new values into respective registers
        // LL_TIM_GenerateEvent_UPDATE(DHT_TIMER);
        // Start timer
        LL_TIM_EnableCounter(DHT_TIMER);
    }

    //!
    static void one_shot(int ms)
    {
        // NOTE: Timer tick in msec
        LL_TIM_SetAutoReload(DHT_TIMER, ms * 1000 / DHT_TIMER_TICK_PERIOD_US);

        // Start counting from 0
        LL_TIM_SetCounter(DHT_TIMER, 0);

        // Trigger update to load new values into respective registers
        LL_TIM_GenerateEvent_UPDATE(DHT_TIMER);

        // Enable update event
        // **BUG** in LL library:
        // According to RM0008, page 403, section 15.4.1 TIMx control register 1
        // (TIMx_CR1) UDIS bit must be reset to let the timer update event be
        // enabled. Instead of clearing that bit, the LL_TIM_EnableUpdateEvent()
        // function sets it.
        CLEAR_BIT(DHT_TIMER->CR1, TIM_CR1_UDIS);

        // Clear update flag to prevent interrupt fire
        LL_TIM_ClearFlag_UPDATE(DHT_TIMER);

        // Enable interrupt as an action for any timer event
        LL_TIM_EnableIT_UPDATE(DHT_TIMER);

        // Start timer
        LL_TIM_EnableCounter(DHT_TIMER);
    }

    //!
    static uint32_t us_from_start()
    {
        // NOTE: Timer tick in msec
        return DHT_TIMER_TICK_PERIOD_US * LL_TIM_GetCounter(DHT_TIMER);
    }

    //--------------------------------------------------------------------------

    //!
    static void check_disable_oneshot()
    {
        // This timer is always oneshot-only
        LL_TIM_DisableCounter(DHT_TIMER);
        LL_TIM_SetCounter(DHT_TIMER, 0);
    }
};

using pc_13_led =
  platform::compat::gpio_pin<platform::compat::gpio_port::c, LL_GPIO_PIN_13>;
using pb_9_dht =
  platform::compat::gpio_pin<platform::compat::gpio_port::b, LL_GPIO_PIN_9>;

using dht_instance = dev::dht11<pb_9_dht, basic_dht11_timer>;

//------------------------------------------------------------------------------
// Main routine

int main()
{
    ll_init();
    clk_config();
    pin_config();
    rtc_init();
    timer_init();

    platform::compat::console_cfg();

    printf("Fugu board starting!\r\n");

    // -------------------------------------------------------------------------
    // DHT11 query

    printf("Start probing DHT11\r\n");

    dht_instance::init();
    dht_instance::start_read();
    platform::compat::delay(2000);

    // Try to get data

    for (int i = 0; i < 5; ++i) {
        if (dht_instance::data_status() == dev::dht11_status::data_ready) {
            int temp = dht_instance::get_temperature();
            int humid = dht_instance::get_humidity();
            printf("temp: %#x, %d.%d\r\n", temp, temp >> 8, temp & 0xff);
            printf("humid: %#x, %d.%d\r\n", humid, humid >> 8, humid & 0xff);
            break;
        } else {
            printf("DHT11 failed to get data\r\n");
            dht_instance::start_read();
            platform::compat::delay(2000);
        }
    }

    // -------------------------------------------------------------------------
    // Record time samples from DHT11 timer and then print them

    printf("DHT timer stop-start test\r\n");

    basic_dht11_timer::start();

    uint32_t time_samples[100];
    for (int i = 0; i < 100; ++i) {
        time_samples[i] = basic_dht11_timer::us_from_start();
        platform::compat::delay(10);
    }

    basic_dht11_timer::stop();

    for (int i = 0; i < 100; ++i) {
        printf("DHT11 driver timer value: %lu mcs\r\n", time_samples[i]);
    }

    // -------------------------------------------------------------------------
    // Blink a LED using timer interrupt

    printf("Timer-based blink 1000 ms period\r\n");

    LL_TIM_EnableCounter(TEST_TIMER);
    platform::compat::delay(2500);
    LL_TIM_DisableCounter(TEST_TIMER);

    // -------------------------------------------------------------------------
    // Infinite loop to show blink and busy-wait delay

    printf("Delay-toggle and RTC test\r\n");

    while (1) {
        platform::compat::delay(300);
        pc_13_led::toggle();
        platform::compat::delay(700);
        pc_13_led::toggle();
        // TODO: count seconds, minutes, hours
        printf("Current time: %lu\r\n", LL_RTC_TIME_Get(RTC));
    }
}

//------------------------------------------------------------------------------
// Cortex-M3 interrupts

//! Non maskable interrupt handler
extern "C" void NMI_Handler(void) {}

//!
extern "C" void HardFault_Handler(void)
{
    while (1) {
    }
}

//!
extern "C" void MemManage_Handler(void)
{
    while (1) {
    }
}

//!
extern "C" void BusFault_Handler(void)
{
    while (1) {
    }
}

//!
extern "C" void UsageFault_Handler(void)
{
    while (1) {
    }
}

//!
extern "C" void SVC_Handler(void) {}

//!
extern "C" void DebugMon_Handler(void) {}

//!
extern "C" void PendSV_Handler(void) {}

//!
extern "C" void SysTick_Handler(void) {}

//------------------------------------------------------------------------------
// STM32F1XX interrupts

//! Basic timer interrupt, specifically dedicated to the DHT11 driver
extern "C" __attribute__((used)) void TEST_TIMER_ISR(void)
{
    LL_TIM_ClearFlag_UPDATE(TEST_TIMER);
    pc_13_led::toggle();
}

//! DH11 timer interrupt
extern "C" __attribute__((used)) void DHT_TIMER_ISR(void)
{
    // Stop counter to prevent subsequent interrupts.
    basic_dht11_timer::check_disable_oneshot();
    dht_instance::event_timer();
}

//! DH11 driver EXTI line, delivering events from PB9 pin
extern "C" __attribute__((used)) void EXTI9_5_IRQHandler(void)
{
    dht_instance::event_gpio();

    // TODO: why this clear is required, despite that event_gpio() already
    // cleared EXTI?
    NVIC_ClearPendingIRQ(EXTI9_5_IRQn);
}
