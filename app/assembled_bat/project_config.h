#ifndef FUGU_PROJECT_CONFIG_H_
#define FUGU_PROJECT_CONFIG_H_

//! MCU model
#define STM32F103xB                         1
//! Use LL driver from STM32 HAL
#define USE_FULL_LL_DRIVER                  1

//! VDD value
#define VDD_VALUE                           3300U
//! Prefetch?
#define PREFETCH_ENABLE                     32768U
//! UART periphery used for console output
#define CONSOLE_UART                        USART1
//! Console baud rate
#define CONSOLE_BAUD                        115200

//------------------------------------------------------------------------------
// Clocks configuration

//! HSE clock value
#define HSE_VALUE                           8000000U
//! HSE startup timeout
#define HSE_STARTUP_TIMEOUT                 100U
//! LSE startup timeout
#define LSE_STARTUP_TIMEOUT                 5000U
//! LSE clock value
#define LSE_VALUE                           32768U
//! HSI clock value
#define HSI_VALUE                           8000000U
//! LSI clock value
#define LSI_VALUE                           40000U

//! System clock source
#define SYSCLOCK_SOURCE                     LL_RCC_SYS_CLKSOURCE_PLL

// Section valid only for PLL
#if SYSCLOCK_SOURCE == LL_RCC_SYS_CLKSOURCE_PLL
    //! PLL HSE clock input divider
    #define PLL_HSE_DIV                     LL_RCC_PLLSOURCE_HSE_DIV_2
    //! PLL clock multiplier
    #define PLL_MUL                         LL_RCC_PLL_MUL_8
#endif

//! AHB clock domain prescaler
#define AHB_PRESCALER                       LL_RCC_SYSCLK_DIV_1
//! APB1 clock domain prescaler
#define APB1_PRESCALER                      LL_RCC_APB1_DIV_1
//! APB2 clock domain prescaler
#define APB2_PRESCALER                      LL_RCC_APB2_DIV_4

//------------------------------------------------------------------------------
// Timer configuration

// Timer-related macro helpers

#define MAKE_TIMER_IRQN(TIMER)              TIMER ## _IRQn
#define MAKE_TIMER_PERIPH_CLK(TIMER)        LL_APB1_GRP1_PERIPH_ ## TIMER
#define MAKE_TIMER_ISR(TIMER)               TIMER ## _IRQHandler

//! Timer for DHT11
#define DHT_TIMER                           TIM3
//! DHT11 timer IRQn
#define DHT_TIMER_IRQn                      MAKE_TIMER_IRQN(TIM3)
//! DHT11 timer tick period, in microseconds
#define DHT_TIMER_TICK_PERIOD_US            1
//! DHT11 timer periphery clock
#define DHT_TIMER_PERIPH_CLK                MAKE_TIMER_PERIPH_CLK(TIM3)
//! DHT11 interrupt routine name
#define DHT_TIMER_ISR                       MAKE_TIMER_ISR(TIM3)

//! Timer for test purposes
#define TEST_TIMER                          TIM2
//! Timer tick period, in microseconds
#define TEST_TIMER_TICK_PERIOD_US           1000
//! Test timer IRQn
#define TEST_TIMER_IRQn                     MAKE_TIMER_IRQN(TIM2)
//! Test timer periphery clock
#define TEST_TIMER_PERIPH_CLK               MAKE_TIMER_PERIPH_CLK(TIM2)
//! Test timer interrupt routine name
#define TEST_TIMER_ISR                      MAKE_TIMER_ISR(TIM2)

#endif // FUGU_PROJECT_CONFIG_H_
