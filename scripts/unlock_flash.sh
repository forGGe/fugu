#!/bin/sh

openocd -f interface/stlink-v2.cfg -f target/stm32f1x.cfg -c "init; flash probe 0; halt; stm32f1x unlock 0; exit;"

# Now reset a board and run st-util