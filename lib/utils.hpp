//! \file
//! \brief

#ifndef LIB_UTILS_HPP_
#define LIB_UTILS_HPP_

#include <type_traits>

namespace lib
{

//! Extracts value from enumerator element of given type.
//! \tparam Enum Enumerator type.
//! \param[in] val Value of the enumerator type.
//! \return Integer representation of the enum value.
template<typename Enum>
constexpr auto extract_value(Enum val)
{
    static_assert(std::is_enum<Enum>::value, "Given type is not enumeration.");

    return static_cast<std::underlying_type_t<Enum>>(val);
}

} // lib

#endif // LIB_UTILS_HPP_